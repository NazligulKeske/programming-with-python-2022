import random
import numpy as np
import math
# Exercise a
a=30
factorial=1
for i in range (1,a+1):
    factorial=factorial*i
result1= factorial%59

print("the result1 is: " ,end="")
print(result1)

#Exercise b

b= 2**100
result2 = b%7

print("the result2 is: " ,end="")
print(result2)

#Exercise c

x = 99* "9"
result3 = int(x)//25

print("the result3 is: " ,end="")
print(result3)

#Exercise d

Y = 33**33
print("the result4 is: " ,end="")
print( Y.bit_length() )

#Exercise e

print("the result5 is: " ,end="")
print(len(str(Y)))

#Exercise f

real_number_list = [-10, 5, 20, -35]
absolute_list =[]

for number in real_number_list:
    absolute_value = abs(number)
    absolute_list.append(absolute_value)
absolute_list.sort()

print("the result6 is: " ,end="")
print(absolute_list[0], absolute_list[-1])

#Exercise g

result7 =  math.pi * (3**2)
print("the result7 is: " ,end="")
print(result7)

#Exercise h

result8 = math.comb(20,10)*math.comb(10,1)*math.comb(10,1)
print("the result8 is: " ,end="")
print(result8)







